<p align="center">
  <a href="https://angular.io/" target="blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/38978999/Angular.png" width="200" alt="Angular Logo" /></a>
</p>

<h1 align="center">Password Manager UI Web</h1>


Repsitory:  <a href="https://gitlab.com/pwd-manager">Password Manager</a><img src="https://driftt.imgix.net/https%3A%2F%2Fdriftt.imgix.net%2Fhttps%253A%252F%252Fs3.us-east-1.amazonaws.com%252Fcustomer-api-avatars-prod%252F85489%252F7e9c21cd7a2d336fbf996efb559b33ce5da2m9wiera6%3Ffit%3Dmax%26fm%3Dpng%26h%3D200%26w%3D200%26s%3Df33d764f537c23e594ddc6316fe21d22?fit=max&fm=png&h=200&w=200&s=f1117f559dc6f92d467aa3f7c6a76a5f"  width="35" height="20">

Author: <a href="https://gitlab.com/juanesquintero">Juanes Quintero</a>

<hr>

## About 
This project is the User interface for password manager based on a project template (https://gitlab.com/juanesquintero/angular-template) uses Angular 14.2, Angular Material & Bootstrap for icons, controls, and styles helpers.

With this UI you will be able to navigate through the app and visualize in an interactive way Cards, create, delete and update Cards easily.

To run the local app you could use the following options:

### Docker (recomended)
1. Make sure you have docker and docker compose installed 
(https://docs.docker.com/engine/install/) 
(Docker version 20.10.13)
1. Clone the parent repo recursively (https://gitlab.com/pwd-manager/password-manager)
2. Run (this command runs the ui and api for hole project)
  ```
   $ docker-compose up dev
  ```

### Local env
1. Make sure you have NodeJS installed
https://nodejs.org/en/download/
(v16.13.1)

2. Run inside of the project folder to install dependencies
   ```
   $ npm install
   ```
3. Start the app with
   ```
   $ npm run start
   ```
   or
   ```
    $ ng serve
    ```

### Testing
If tou want to run unit tests, it has to be locally and not inside the container since Chrome is required for Karma, so install the dependencies locally for test propouses...

```
$ npm i 
```

...and run tests with:

```
$ ng test 
```

or 

```
$ npm run test 
```
 
 <br>
 <br>

```
$ docker exec ui_pwd-mngr_dev npm run test
```


<br>
<hr>
<br>


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
