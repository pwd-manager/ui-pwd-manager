import { Component, OnInit } from '@angular/core';
import { APP_NAME } from '@shared/constants';

@Component({
  selector: 'pm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public title: string = APP_NAME;

  constructor() { }

  ngOnInit(): void {}
}
