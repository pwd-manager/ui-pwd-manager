import { Component, OnInit } from '@angular/core';
import { MENU_OPTIONS } from '@shared/constants';
import { IOption } from '@shared/models/index';

@Component({
  selector: 'pm-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SideBarComponent implements OnInit {
  public options: IOption[] = MENU_OPTIONS;
  
  constructor() { }

  ngOnInit(): void {
  }

}
