import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppFormsModule } from '@shared/forms.module';
import { MaterialModule } from '@shared/material.module';
import { AuthService } from '@core/services/auth/auth.service';
import { HeaderComponent } from './components/header/header.component';
import { SideBarComponent } from './components/sidebar/sidebar.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SideBarComponent
  ],
  exports: [
    HeaderComponent,
    SideBarComponent
  ],
  providers:[
    AuthService
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AppFormsModule,
    RouterModule
  ]
})
export class CoreModule { }
