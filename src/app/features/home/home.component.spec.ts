import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';

const router = {
  navigate: jasmine.createSpy('navigate'),
}


describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let compiled: HTMLElement;
  let componentHTML: string | undefined;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        HomeComponent
      ],
      providers: [ {provide: Router, useValue: router} ],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
    componentHTML = compiled.querySelector('.home')?.innerHTML;
  });

  it(`should render 'explore cards' option`, () => {
    expect(componentHTML).toContain('<strong>Explore Cards</strong>');
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have 'ngOnInit' method`, () => {
    expect(component.ngOnInit).toBeTruthy();
  });

  describe('goToCards()', () => {
    it(`should navigate to 'cards' with router`, () => {
      router.navigate.calls.reset();
      component.goToCards()
      expect(router.navigate).toHaveBeenCalledOnceWith(['cards']);
    });
  });
});
