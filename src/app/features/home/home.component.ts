import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { APP_NAME } from '@shared/constants';

@Component({
  selector: 'pm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  public title: string = APP_NAME;

  constructor(private router: Router) { }

  ngOnInit(): void { }

  goToCards(): void {
    this.router.navigate(['cards'])
  }
}
