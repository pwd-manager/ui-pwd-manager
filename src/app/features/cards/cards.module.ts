import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/material.module';
import { AppFormsModule } from '@shared/forms.module';
import { CardsRoutingModule } from './cards-routing.module';
import { CardsService } from './services/cards.service';
import { CardItemComponent } from './components/item/card-item.component';
import { CardsListComponent } from './components/list/cards-list.component';
import { CardModalComponent } from './components/modal/card-modal.component';
import { ClipboardModule } from '@angular/cdk/clipboard';


@NgModule({
  declarations: [
    CardsListComponent,
    CardModalComponent,
    CardItemComponent
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    MaterialModule,
    AppFormsModule,
    ClipboardModule
  ],
  providers: [CardsService]
})
export class CardsModule { }
