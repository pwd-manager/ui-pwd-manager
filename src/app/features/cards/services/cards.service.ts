import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { APP_ROUTES } from '@shared/constants';
import { ICardDTO, IResponse } from '@shared/models/cards.model';

@Injectable()
export class CardsService {
  private endpoint: string = environment.apiPath + '/' + APP_ROUTES.cards;

  constructor(private http: HttpClient) { }

  getList(textFragment?: string ): Observable<IResponse> {
    const params = JSON.parse(JSON.stringify({ textFragment }));
    return this.http.get<IResponse>(this.endpoint, { params });
  }

  getOne(id: number): Observable<IResponse> {
    return this.http.get<IResponse>(this.endpoint + '/' + id);
  }

  postOne(cardData: ICardDTO): Observable<IResponse> {
    return this.http.post<IResponse>(this.endpoint, cardData);
  }

  putOne(id: number, cardData: ICardDTO): Observable<IResponse> {
    return this.http.put<IResponse>(this.endpoint + '/' + id, cardData);
  }

  deleteOne(id: number): Observable<IResponse> {
    return this.http.delete<IResponse>(this.endpoint + '/' + id);
  }
}
