import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { CardsService } from './cards.service';

describe('CardsService', () => {
  let service: CardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ CardsService ],
    });
    service = TestBed.inject(CardsService);
  });

  it(`should have 'getList' method`, () => {
    expect(service.getList).toBeTruthy();
  });

  it(`should have 'getOne' method`, () => {
    expect(service.getOne).toBeTruthy();
  });

  it(`should have 'postOneº' method`, () => {
    expect(service.postOne).toBeTruthy();
  });

  it(`should have 'putOne' method`, () => {
    expect(service.putOne).toBeTruthy();
  });

  it(`should have 'deleteOne' method`, () => {
    expect(service.deleteOne).toBeTruthy();
  });
});
