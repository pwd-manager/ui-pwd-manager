import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsListComponent } from './components/list/cards-list.component';

const cardsRoutes: Routes = [
  {
    path: '',
    runGuardsAndResolvers: 'always',
    component: CardsListComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(cardsRoutes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
