import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { cardMock } from '@shared/mocks/card.mock';
import { CardsService } from '@cards/services/cards.service';
import { CardItemComponent } from './card-item.component';

const service: jasmine.SpyObj<CardsService> = jasmine.createSpyObj('CardsService', {
  getList: of([cardMock])
});

describe('CardItemComponent', () => {
  let component: CardItemComponent;
  let fixture: ComponentFixture<CardItemComponent>;
  let compiled: HTMLElement;
  let componentHTML: string | undefined;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardItemComponent ],
      providers: [
        {
          provide: CardsService,
          useValue: service,
        },
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardItemComponent);
    component = fixture.componentInstance;
    component.card = cardMock;
    compiled = fixture.nativeElement as HTMLElement;
    componentHTML = compiled.querySelector('.card-item')?.innerHTML;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should render 'launch' option`, () => {
    expect(componentHTML).toContain('Lauch</small>');
  });

  it('should have OnInit method', () => {
    expect(component.ngOnInit).toBeTruthy();
  });

  describe('onOpenModal()', () => {
    it('should call emit output', () => {
      const spy = spyOn(component.openModal, 'emit');
      component.onOpenModal();
      expect(spy).toHaveBeenCalled();
    });
  })

  describe('onDelete()', () => {
    it(`should call 'fetchCards'`, () => {
      const spy = spyOn(window, 'confirm');
      component.onDelete();
      expect(spy).toHaveBeenCalled();
    });
  });
});
