import { CardsService } from '@cards/services/cards.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICard, IResponse } from '@shared/models/cards.model';

@Component({
  selector: 'pm-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent implements OnInit {
  @Input() public card!: ICard;
  @Output() openModal: EventEmitter<ICard> = new EventEmitter();
  @Output() refresh: EventEmitter<boolean> = new EventEmitter();

  constructor(private cardsService: CardsService) { }

  ngOnInit(): void { }

  onOpenModal(){
    this.openModal.emit(this.card);
  }

  onDelete(): void {
    const { id, name } = this.card;
    if (window.confirm(`Delete "${name}" card?`)) {
      this.cardsService.deleteOne(id).subscribe((res: IResponse) => {
        alert(`Deleted ${id} "${name}" card`);
        this.refresh.emit();
      });
    }
  }
}
