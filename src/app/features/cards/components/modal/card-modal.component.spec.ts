import { of } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { cardMock } from '@shared/mocks/card.mock';
import { CardsService } from '@cards/services/cards.service';
import { CardModalComponent } from './card-modal.component';

let dialogRef: jasmine.SpyObj<MatDialogRef<CardModalComponent>> = jasmine.createSpyObj('MatDialogRef', {
  open: {
    afterClosed: () => ({
      subscribe: (fn: any) => { }
    })
  },
  close: () => { },
});

const service: jasmine.SpyObj<CardsService> = jasmine.createSpyObj('CardsService', {
  getList: of({data:[cardMock]}),
  putOne: of({data: cardMock}),
  postOne: of({data: cardMock}),
});

describe('CardModalComponent', () => {
  let component: CardModalComponent;
  let fixture: ComponentFixture<CardModalComponent>;
  let compiled: HTMLElement;
  let componentHTML: string | undefined;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardModalComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: { card: {} }
        },
        {
          provide: CardsService,
          useValue: service,
        },
      ],
    })
      .compileComponents();

    fixture = TestBed.createComponent(CardModalComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
    componentHTML = compiled.querySelector('.card-modal')?.innerHTML;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it(`should render label for card fileds option`, () => {
    const cardFields = Object.keys(cardMock);
    cardFields.shift();
    for (const filed of cardFields) {
      expect(componentHTML?.toLowerCase()).toContain(`>${filed}</mat-label>`.toLowerCase());
    }
  });

  describe('ngOnInit()', () => {
    it('should call initForm() service', () => {
      const spy = spyOn(component, 'initForm');
      component.ngOnInit();
      expect(spy).toHaveBeenCalled();
    });
  })

  describe('getter: isNew()', () => {
    it(`should false is card is defined`, () => {
      component.card = cardMock;
      expect(component.isNew).toBe(false);
    });

    it(`should true is card is not defined`, () => {
      component.card = undefined;
      expect(component.isNew).toBe(true);
    });
  })

  describe('onSubmit()', () => {
    beforeEach(() => {
      Object.defineProperty(component.form, 'valid', {
        get: () => true
      });
    });

    it(`should call create if 'isNew'`, () => {
      spyOn(component, 'create');
      component.card = undefined;
      component.onSubmit();
      expect(component.create).toHaveBeenCalled();
    });

    it(`should call update if not 'isNew'`, () => {
      spyOn(component, 'update');
      component.card = cardMock;
      component.onSubmit();
      expect(component.update).toHaveBeenCalled();
    });
  })

  describe('update()', () => {
    it(`should call 'putOne' service`, () => {
      component.update();
      expect(service.putOne).toHaveBeenCalled();
    });
  })

  describe('create()', () => {
    it(`should call 'postOne' service`, () => {
      component.create();
      expect(service.postOne).toHaveBeenCalled();
    });
  })

  describe('onClose()', () => {
    it(`should 'close' dialog modal`, () => {
      component.onClose();
      expect(dialogRef.close).toHaveBeenCalled();
    });
  })
});
