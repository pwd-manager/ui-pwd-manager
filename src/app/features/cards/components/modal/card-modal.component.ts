import { CardsService } from '@cards/services/cards.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICard, IResponse } from '@shared/models/cards.model';

@Component({
  selector: 'pm-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.scss']
})
export class CardModalComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public hide?: boolean = false;

  get isNew(): boolean {
    return !this.card;
  }

  constructor(
    private cardsService: CardsService,
    public dialogRef: MatDialogRef<CardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public card?: ICard,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      name: new FormControl({ value: this.card?.name, disabled: false }, Validators.required),
      url: new FormControl({ value: this.card?.url, disabled: false }, Validators.required),
      username: new FormControl({ value: this.card?.username, disabled: false }, Validators.required),
      password: new FormControl({ value: this.card?.password, disabled: false }, Validators.required),
    })
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.isNew) {
        this.create()
      } else {
        this.update()
      }
    }
    this.dialogRef.close(true);
  }

  update(): void {
    if (this.card) {
      const { id } = this.card;
      this.cardsService.putOne(id, this.form.value).subscribe((res: IResponse) => {
        alert(`Updated "${(res.data as ICard).name}" card`);
      });
    }
  }

  create(): void {
    this.cardsService.postOne(this.form.value).subscribe((res: IResponse) => {
      alert(`Created "${(res.data as ICard).name}" card`);
    });
  }

  onClose() {
    this.dialogRef.close()
  }
}
