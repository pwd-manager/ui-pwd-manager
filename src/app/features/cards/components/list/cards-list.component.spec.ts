import { cardMock } from './../../../../shared/mocks/card.mock';
import { of } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardsListComponent } from './cards-list.component';
import { CardsService } from '@cards/services/cards.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardModalComponent } from '../modal/card-modal.component';

const dialog: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('MatDialog', {
  open: jasmine.createSpyObj('MatDialogRef', {
    afterClosed: of({
      card: {}, action: ''
    })
  })
});
const snackbar: jasmine.SpyObj<MatSnackBar> = jasmine.createSpyObj('MatSnackBar', {
  open: {}
});
const service: jasmine.SpyObj<CardsService> = jasmine.createSpyObj('CardsService', {
  getList: of([cardMock])
});

describe('CardsListComponent', () => {
  let component: CardsListComponent;
  let fixture: ComponentFixture<CardsListComponent>;
  let compiled: HTMLElement;
  let componentHTML: string | undefined;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardsListComponent],
      providers: [
        {
          provide: CardsService,
          useValue: service,
        },
        {
          provide: MatDialog,
          useValue: dialog,
        },
        {
          provide: MatSnackBar,
          useValue: snackbar,
        },
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CardsListComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
    componentHTML = compiled.querySelector('.cards-list')?.innerHTML;

    fixture.detectChanges();
  });

  it(`should render 'Search' option`, () => {
    expect(componentHTML).toContain('Search</button>');
  });


  describe('ngOnInit()', () => {
    it('should call fetchCards() service', () => {
      const spy = spyOn(component, 'fetchCards');
      component.ngOnInit();
      expect(spy).toHaveBeenCalled();
    });
  })

  describe('fetchCards()', () => {
    it('should call getList() service', () => {
      component.fetchCards();
      expect(service.getList).toHaveBeenCalled();
    });
  })

  describe('onOpenModal()', () => {
    it(`should open dialog modal`, () => {
      component.onOpenModal(cardMock);
      expect(dialog.open).toHaveBeenCalled();
    });

    it(`should call 'onCloseModal' method`, () => {
      const spy = spyOn(component, 'onCloseModal');
      component.onOpenModal(cardMock);
      expect(spy).toHaveBeenCalled();
    });
  })

  describe('onCloseModal()', () => {
    let dialogRef: any;
    beforeEach(() => {
      dialogRef = dialog.open(CardModalComponent, {});
    })

    it(`should call 'afterClosed'`, () => {
      component.onCloseModal(dialogRef);
      expect(dialogRef.afterClosed).toHaveBeenCalled();
    });
  });

  describe('onSearch()', () => {
    it(`should call 'fetchCards'`, () => {
      const spy = spyOn(component, 'fetchCards');
      component.onSearch();
      expect(spy).toHaveBeenCalledWith(component.searchText);
    });
  });
});
