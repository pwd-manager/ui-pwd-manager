import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ICard, IResponse } from '@shared/models/cards.model';
import { CardsService } from '@cards/services/cards.service';
import { APP_ROUTES } from '@shared/constants';
import { CardModalComponent } from '../modal/card-modal.component';

@Component({
  selector: 'pm-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {
  public title: string = APP_ROUTES.cards;
  public cards: ICard[] = [];
  public searchText?: string;

  constructor(
    private cardsService: CardsService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.fetchCards();
  }

  fetchCards(textFragment?: string): void {
    this.cardsService.getList(textFragment).subscribe((res: IResponse) => {
      this.cards = res.data as ICard[];
    });
  }

  onOpenModal(card?: ICard): void {
    const dialogRef = this.dialog.open(
      CardModalComponent,
      {
        width: '36rem',
        data: card
      });
    this.onCloseModal(dialogRef);
  }

  onCloseModal(dialogRef: MatDialogRef<CardModalComponent>): void {
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.fetchCards();
      }
    });
  }

  onSearch() {
    this.fetchCards(this.searchText);
  }
}
