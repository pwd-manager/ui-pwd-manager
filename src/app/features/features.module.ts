import { HomeComponent } from '@features/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/material.module';
import { AppFormsModule } from '@shared/forms.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  exports: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AppFormsModule,
  ],
  providers: [],
})
export class FeaturesModule { }
