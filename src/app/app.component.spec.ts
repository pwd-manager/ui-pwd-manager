import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

let dialog: jasmine.SpyObj<MatDialog>;
dialog = jasmine.createSpyObj('MatDialog', {
  open: {
    afterClosed: () => ({
      subscribe: (fn: any) => { }
    })
  }
});

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: MatDialog,
          useValue: dialog,
        },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;

    fixture.detectChanges();
  });

  it('should create the app component', () => {
    expect(app).toBeTruthy();
  });
});
