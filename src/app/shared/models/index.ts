export interface IModalOptions {
  mode: string;
  action?: any;
  message?: string;
}

export type IModalMode = 'edit' | 'remove' | 'new' | 'detail';

export type IAction = 'update' | 'create' | 'delete';

export interface IOption {
  label: string;
  routerLink: string;
}

export interface APIResponse<T> {
  detail: string;
  data: T | T[] | null | [];
  count?: number| null;
}
