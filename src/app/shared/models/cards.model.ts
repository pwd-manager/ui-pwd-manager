import { APIResponse } from '@shared/models';

export interface ICardDTO {
  url:	string;
  name: string;
  username: string;
  password: string;
}
export interface ICard extends ICardDTO {
  id: number;
}

export type IResponse = APIResponse<ICard>;

export type CardField = keyof ICardDTO;

