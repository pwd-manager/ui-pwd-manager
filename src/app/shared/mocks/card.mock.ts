import { ICard } from "../models/cards.model";

export const cardMock: ICard = {
  id: 123,
  name: 'Card Title',
  url: 'http://',
  username: 'jhon_doe',
  password: '3123123',
}
