import { IAction, IOption } from './models';

export enum APP_ROUTES {
  cards='cards'
};

export const MENU_OPTIONS: IOption[] = [
  { label: 'Cards', routerLink: APP_ROUTES.cards },
  { label: 'Passwords', routerLink: '#' },
  { label: 'Notes', routerLink: '#' },
  { label: 'Addresses', routerLink: '#' },
  { label: 'Payment Cards', routerLink: '#' },
  { label: 'Banking Accounts', routerLink: '#' },
];

export const ACTIONS_LABEL: Record<string, IAction> = {
  edit: 'update',
  new: 'create',
  remove: 'delete',
};

export const APP_NAME = 'password-manager';
