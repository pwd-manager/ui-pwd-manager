export const title = (text: string) => {
  return text[0].toUpperCase() + text.slice(1);
};

export const randomInRange = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min) + min);
}
