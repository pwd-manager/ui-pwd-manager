import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from '@shared/material.module';
import { AppComponent } from './app.component';
import { AppFormsModule } from '@shared/forms.module';
import { FeaturesModule } from '@features/features.module';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppFormsModule,
    FeaturesModule,
    CoreModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
